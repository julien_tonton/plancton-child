<?php
add_action('after_setup_theme', 'uncode_language_setup');
function uncode_language_setup()
{
	load_child_theme_textdomain('uncode', get_stylesheet_directory() . '/languages');
}

function theme_enqueue_styles()
{
	$production_mode = ot_get_option('_uncode_production');
	$resources_version = ($production_mode === 'on') ? null : rand();
	$parent_style = 'uncode-style';
	$child_style = array('uncode-custom-style');
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/library/css/style.css', array(), $resources_version);
	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', $child_style, $resources_version);
}
add_action('wp_enqueue_scripts', 'theme_enqueue_styles');


/* ----------------------------------- */
// REGISTER CUSTOM POST TYPE ORGANISME //
/* ----------------------------------- */
/* ----------------------------------- */
/* -You Need Register also Post type in Uncode Core plugin - portfolio_category taxo... */

if ( ! function_exists('custom_post_organisme') ) {

// Register Custom Post Type
function custom_post_organisme() {

	$labels = array(
		'name'                  => _x( 'Organismes', 'Post Type General Name', 'uncode' ),
		'singular_name'         => _x( 'Organisme', 'Post Type Singular Name', 'uncode' ),
		'menu_name'             => __( 'Organisme', 'uncode' ),
		'name_admin_bar'        => __( 'Organisme', 'uncode' ),
		'archives'              => __( 'Organisme Archives', 'uncode' ),
		'parent_item_colon'     => __( 'Parent Item:', 'uncode' ),
		'all_items'             => __( 'All Organismes', 'uncode' ),
		'add_new_item'          => __( 'Add New Organisme', 'uncode' ),
		'add_new'               => __( 'Add New', 'uncode' ),
		'new_item'              => __( 'New Organisme', 'uncode' ),
		'edit_item'             => __( 'Edit Organisme', 'uncode' ),
		'update_item'           => __( 'Update Organisme', 'uncode' ),
		'view_item'             => __( 'View Organisme', 'uncode' ),
		'search_items'          => __( 'Search Organisme', 'uncode' ),
		'not_found'             => __( 'Not found', 'uncode' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'uncode' ),
		'featured_image'        => __( 'Featured Image', 'uncode' ),
		'set_featured_image'    => __( 'Set featured image', 'uncode' ),
		'remove_featured_image' => __( 'Remove featured image', 'uncode' ),
		'use_featured_image'    => __( 'Use as featured image', 'uncode' ),
		'insert_into_item'      => __( 'Insert into item', 'uncode' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'uncode' ),
		'items_list'            => __( 'Items list', 'uncode' ),
		'items_list_navigation' => __( 'Items list navigation', 'uncode' ),
		'filter_items_list'     => __( 'Filter items list', 'uncode' ),
	);
	$args = array(
		'label'                 => __( 'Organisme', 'uncode' ),
		'description'           => __( 'Organismes', 'uncode' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
		'taxonomies'            => array( 'portfolio_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-carrot',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'organisme', $args );

}
add_action( 'init', 'custom_post_organisme', 0 );

}
