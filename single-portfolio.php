<?php

/**
 * The Template for displaying all single portfolio.
 *
 * @package uncode
 */

get_header();

/**
 * DATA COLLECTION - START
 *
 */

/** Init variables **/
$limit_width = $limit_content_width = $the_content = $main_content = $media_content = $info_content = $navigation_content = $layout = $portfolio_style = $portfolio_bg_color = $sidebar = $sidebar_size = $sidebar_padding = $sidebar_inner_padding = $sidebar_content = $title_content = $page_custom_width = $row_classes = $main_classes = $media_classes = $info_classes = '';
$with_builder = false;

$post_type = 'portfolio';

/** Get general datas **/
if (isset($metabox_data['_uncode_specific_style'][0]) && $metabox_data['_uncode_specific_style'][0] !== '') {
	$style = $metabox_data['_uncode_specific_style'][0];
	if (isset($metabox_data['_uncode_specific_bg_color'][0]) && $metabox_data['_uncode_specific_bg_color'][0] !== '') {
		$bg_color = $metabox_data['_uncode_specific_bg_color'][0];
	}
} else {
	$style = ot_get_option('_uncode_general_style');
	if (isset($metabox_data['_uncode_specific_bg_color'][0]) && $metabox_data['_uncode_specific_bg_color'][0] !== '') {
		$bg_color = $metabox_data['_uncode_specific_bg_color'][0];
	} else $bg_color = ot_get_option('_uncode_general_bg_color');
}
$bg_color = ($bg_color == '') ? ' style-'.$style.'-bg' : ' style-'.$bg_color.'-bg';

/** Get page width info **/
$boxed = ot_get_option('_uncode_boxed');

if ($boxed !== 'on')
{
	$page_content_full = (isset($metabox_data['_uncode_specific_layout_width'][0])) ? $metabox_data['_uncode_specific_layout_width'][0] : '';
	if ($page_content_full === '')
	{

		/** Use generic page width **/
		$generic_content_full = ot_get_option('_uncode_' . $post_type . '_layout_width');
		if ($generic_content_full === '')
		{
			$main_content_full = ot_get_option('_uncode_body_full');
			if ($main_content_full === '' || $main_content_full === 'off') $limit_content_width = ' limit-width';
		}
		else
		{
			if ($generic_content_full === 'limit')
			{
				$generic_custom_width = ot_get_option('_uncode_' . $post_type . '_layout_width_custom');
				if (is_array($generic_custom_width) && !empty($generic_custom_width))
				{
					$page_custom_width = ' style="max-width: ' . implode("", $generic_custom_width) . ';"';
				}
			}
		}
	}
	else
	{

		/** Override page width **/
		if ($page_content_full === 'limit')
		{
			$limit_content_width = ' limit-width';
			$page_custom_width = (isset($metabox_data['_uncode_specific_layout_width_custom'][0])) ? unserialize($metabox_data['_uncode_specific_layout_width_custom'][0]) : '';
			if (is_array($page_custom_width) && !empty($page_custom_width))
			{
				$page_custom_width = ' style="max-width: ' . implode("", $page_custom_width) . ';"';
			}
		}
	}
}

$media = get_post_meta($post->ID, '_uncode_featured_media', 1);
$media_display = get_post_meta($post->ID, '_uncode_featured_media_display', 1);

$featured_image = get_post_thumbnail_id($post->ID);

/** Collect header data **/
if (isset($metabox_data['_uncode_header_type'][0]) && $metabox_data['_uncode_header_type'][0] !== '')
{
	$page_header_type = $metabox_data['_uncode_header_type'][0];
	if ($page_header_type !== 'none')
	{
		$meta_data = uncode_get_specific_header_data($metabox_data, $post_type, $featured_image);
		$metabox_data = $meta_data['meta'];
		$show_title = $meta_data['show_title'];
	}
}
else
{
	$page_header_type = ot_get_option('_uncode_' . $post_type . '_header');
	if ($page_header_type !== '' && $page_header_type !== 'none')
	{
		$metabox_data['_uncode_header_type'] = array(
			$page_header_type
		);
		$meta_data = uncode_get_general_header_data($metabox_data, $post_type, $featured_image);
		$metabox_data = $meta_data['meta'];
		$show_title = $meta_data['show_title'];
	}
}

/** Get layout info **/
if (isset($metabox_data['_uncode_portfolio_active'][0]) && $metabox_data['_uncode_portfolio_active'][0] !== '')
{

	/** Page specific info **/
	if ($metabox_data['_uncode_portfolio_active'][0] !== 'off')
	{
		$layout = (isset($metabox_data['_uncode_portfolio_position'][0])) ? $metabox_data['_uncode_portfolio_position'][0] : '';
		$sidebar_size = (isset($metabox_data['_uncode_portfolio_sidebar_size'][0])) ? $metabox_data['_uncode_portfolio_sidebar_size'][0] : 4;
		$sidebar_fill = (isset($metabox_data['_uncode_portfolio_sidebar_fill'][0])) ? $metabox_data['_uncode_portfolio_sidebar_fill'][0] : '';
		$portfolio_style = (isset($metabox_data['_uncode_portfolio_style'][0])) ? $metabox_data['_uncode_portfolio_style'][0] : $style;
		$portfolio_bg_color = (isset($metabox_data['_uncode_portfolio_bgcolor'][0]) && $metabox_data['_uncode_portfolio_bgcolor'][0] !== '') ? ' style-' . $metabox_data['_uncode_portfolio_bgcolor'][0] . '-bg' : '';
	}
}
else
{

	/** Page generic info **/
	$layout = ot_get_option('_uncode_' . $post_type . '_position');
	if ($layout !== 'off')
	{
		$portfolio_style = ot_get_option('_uncode_' . $post_type . '_style');
		$sidebar_size = ot_get_option('_uncode_' . $post_type . '_size');
		$sidebar_fill = ot_get_option('_uncode_' . $post_type . '_sidebar_fill');
		$portfolio_bg_color = ot_get_option('_uncode_' . $post_type . '_bgcolor');
		$portfolio_bg_color = ($portfolio_bg_color !== '') ? ' style-' . $portfolio_bg_color . '-bg' : '';
	}
}
if ($portfolio_style === '') $portfolio_style = $style;

/** Get breadcrumb info **/
$generic_breadcrumb = ot_get_option('_uncode_' . $post_type . '_breadcrumb');
$page_breadcrumb = (isset($metabox_data['_uncode_specific_breadcrumb'][0])) ? $metabox_data['_uncode_specific_breadcrumb'][0] : '';
if ($page_breadcrumb === '')
{
	$breadcrumb_align = ot_get_option('_uncode_' . $post_type . '_breadcrumb_align');
	$show_breadcrumb = ($generic_breadcrumb === 'off') ? false : true;
}
else
{
	$breadcrumb_align = (isset($metabox_data['_uncode_specific_breadcrumb_align'][0])) ? $metabox_data['_uncode_specific_breadcrumb_align'][0] : '';
	$show_breadcrumb = ($page_breadcrumb === 'off') ? false : true;
}

/** Get title info **/
$generic_show_title = ot_get_option('_uncode_' . $post_type . '_title');
$page_show_title = (isset($metabox_data['_uncode_specific_title'][0])) ? $metabox_data['_uncode_specific_title'][0] : '';
if ($page_show_title === '')
{
	$show_title = ($generic_show_title === 'off') ? false : true;
}
else
{
	$show_title = ($page_show_title === 'off') ? false : true;
}

/** Get media info **/
$generic_show_media = ot_get_option('_uncode_' . $post_type . '_media');
$page_show_media = (isset($metabox_data['_uncode_specific_media'][0])) ? $metabox_data['_uncode_specific_media'][0] : '';
if ($page_show_media === '')
{
	$show_media = ($generic_show_media === 'off') ? false : true;
}
else
{
	$show_media = ($page_show_media === 'off') ? false : true;
}

/**
 * DATA COLLECTION - END
 *
 */

$portfolio_details = ot_get_option('_uncode_portfolio_details');

if (!empty($portfolio_details))
{

	// The Regular Expression filter
	$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	foreach ($portfolio_details as $key => $value)
	{
		$portfolio_detail = (isset($metabox_data[$value['_uncode_portfolio_detail_unique_id']][0])) ? $metabox_data[$value['_uncode_portfolio_detail_unique_id']][0] : '';
		if ($portfolio_detail !== '')
		{

			// Check if there is a url in the text
			if (preg_match($reg_exUrl, $portfolio_detail, $url))
			{
				$get_host = parse_url($portfolio_detail);
				if (isset($get_host['host'])) $get_host = $get_host['host'];
				// make the urls hyper links
				$portfolio_detail = preg_replace($reg_exUrl, '<a href="' . $url[0] . '">' . $get_host . '</a>', $portfolio_detail);
			}
			$info_content.= '<span class="detail-container"><span class="detail-label">' . $value['title'] . '</span><span class="detail-value">' . $portfolio_detail . '</span></span>';
		}
	}
	if ($info_content !== '') $info_content = '<p>' . $info_content . '</p>';
}

while (have_posts()):
	the_post();

	/** Build header **/
	if ($page_header_type !== '' && $page_header_type !== 'none')
	{
		$page_header = new unheader($metabox_data, $post->post_title);

		$header_html = $page_header->html;
		if ($header_html !== '') {
			echo '<div id="page-header">';
			echo do_shortcode( shortcode_unautop( $page_header->html ) );
			echo '</div>';
		}
	}
	echo '<script type="text/javascript">UNCODE.initHeader();</script>';
	/** Build breadcrumb **/

	if ($show_breadcrumb && !is_front_page() && !is_home())
	{
		if ($breadcrumb_align !== '') $breadcrumb_align = ' text-' . $breadcrumb_align;
		else
		{
			$breadcrumb_align = ' text-right';
		}
		$content_breadcrumb = uncode_breadcrumbs();
		$breadcrumb_title = '<h5 class="breadcrumb-title">' . get_the_title() . '</h5>';
		echo uncode_get_row_template(($breadcrumb_align === 'left' ? $content_breadcrumb . $breadcrumb_title : $breadcrumb_title . $content_breadcrumb) , '', $limit_content_width, $style, ' row-breadcrumb row-breadcrumb-' . $style . $breadcrumb_align, 'half', true, 'half');
	}

	/** Build title **/

	if ($show_title)
	{
		$title_content = '<div class="post-title-wrapper"><h1 class="post-title">' . get_the_title() . '</h1></div>';
	}

	/** Build content **/

	$the_content = get_the_content();
	if (has_shortcode($the_content, 'vc_row')) $with_builder = true;

	/** Default UNCODE Portfolio content **/
	//$the_content = apply_filters('the_content', $the_content);

	/** --------------------------------------------------------------------- **/	
	/** Custom Portfolio Single by TONTON for Plancton **/

	$featured_media_html = '';
	$video_transp = '';

	if(get_field('video_transp1')!=''){
		$video_transp = get_field('video_transp1');
		$featured_media_html = "<div class='video_trans_wrapper mobile-hidden animate_when_almost_visible alpha-anim start_animation'><video id='video-episode' autoplay='' src=".$video_transp['url']." loop='' style='opacity: 1; -webkit-filter: blur(0px); z-index: 1;'></video>";
		if(get_post_thumbnail_id($post->ID)!=''){
			$featured_img = get_post_thumbnail_id($post->ID);
			$featured_image_ID = wp_get_attachment_image_src($featured_img, 'full');
			$featured_image_URL = (string) $featured_image_ID[0];
			$featured_media_html .= "<img id='video-fallback-episode' src=".$featured_image_URL." class='video-on' style='display:none;height:0;width:0;margin:0;'>";
		}
		$featured_media_html .= "</div>";

	}else{
		if(get_post_thumbnail_id($post->ID)!=''){
			$featured_media_html = "<div class='video_trans_wrapper mobile-hidden animate_when_almost_visible alpha-anim start_animation'>";
			$featured_img = get_post_thumbnail_id($post->ID);
			$featured_image_ID = wp_get_attachment_image_src($featured_img, 'full');
			$featured_image_URL = (string) $featured_image_ID[0];
			$featured_media_html .= "<img id='video-fallback-episode' src=".$featured_image_URL." class='video-off'>";
			$featured_media_html .= "</div>";
		}
	}
	// Photo Gallery build
	$def_lang =apply_filters( 'wpml_default_language' , NULL  ) ;
	$curr_lang = apply_filters( 'wpml_current_language' , NULL );
	do_action( 'wpml_switch_language', $def_lang );
	$images = get_field('galerie_photos');
	do_action( 'wpml_switch_language', $curr_lang );
	
	$galerie_photos='';
	if( $images ):
	    foreach( $images as $image ):
	        $galerie_photos.= apply_filters( 'wpml_object_id', $image['ID'], 'attachment' ,true, $curr_lang ).',';
	    endforeach;
	endif;
	$galerie_photos = trim( $galerie_photos, ",");
	// End Photo Gallery build
		
	/** Related organisms based on episode cateogries **/
	$episode_related_data = $cats_related ='';
	$categories = get_the_terms( get_the_ID(), 'portfolio_category');
	if($categories){
		$episode_related_data= '';
		
		foreach($categories as $category) {
			//$episode_related_data.= '<li>';
			$episode_related_data.= '[vc_accordion_tab title="' . $category->name . '"]';
			$episode_related_data.= '[vc_column_text]<ul class="organismes-list">';
			
			$cats_related .= $category->term_id.',';
			
			$posts_array = get_posts(
			    array(
			        'posts_per_page' => -1,
			        'post_type' => 'organisme',
			        'suppress_filters' => '0',
			        'tax_query' => array(
			            array(
			                'taxonomy' => 'portfolio_category',
			                'field' => 'term_id',
			                'terms' => $category->term_id,
			            )
			        )
			    )
			);				
			foreach ( $posts_array as $post ){
				$episode_related_data.= '<li>';
					$episode_related_data.= get_the_post_thumbnail($post->ID);
					$episode_related_data.= '<div class="org-wrapper">';
					$episode_related_data.= '<span class="org-title">'.get_the_title($post->ID).'</span>';
					$episode_related_data.= '<span class="org-sc-title">'.get_field('nom_scienti').'</span>';
					$episode_related_data.= '</div>';
				$episode_related_data.= '</li>';
			}
			$episode_related_data.= '[/vc_column_text][/vc_accordion_tab]';
			//$episode_related_data.= '</li>';
		} 
		$episode_related_data.= '';
	}
	$cats_related = trim( $cats_related, ",");
	wp_reset_postdata();  
	
	/** Build and display navigation html **/
	$navigation_option = ot_get_option('_uncode_' . $post_type . '_navigation_activate');
	if ($navigation_option !== 'off')
	{
		$navigation_index = ot_get_option('_uncode_' . $post_type . '_navigation_index');
		if ($navigation_index !== '')
		{
			$navigation_index_label = ot_get_option('_uncode_' . $post_type . '_navigation_index_label');
			$navigation_index_link = get_permalink($navigation_index);
			$navigation_index_btn = '<a class="btn btn-link text-default-color" href="' . esc_url($navigation_index_link) . '">' . ($navigation_index_label === '' ? esc_html__('Back', 'uncode') : esc_html($navigation_index_label)) . '</a>';
		}
		else $navigation_index_btn = '';
		$navigation = uncode_post_navigation($navigation_index_btn);
		if (!empty($navigation) && $navigation !== '') $navigation_content = uncode_get_row_template($navigation, '', $limit_content_width, $style, ' row-navigation row-navigation-' . $style, true, true, true);
	}
	
	/** Credits for Team members and Roles **/
	$the_team_data='';
	if(get_field('t_production')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Production').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_production').'</strong></span>
	
		';
	}
	if(get_field('t_idee')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Original Idea').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_idee').'</strong></span>
	
		';
	}
	
	if(get_field('t_realisation')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Director').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_realisation').'</strong></span>
	
		';
	}
	if(get_field('t_conseille')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Scientific consultant').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_conseille').'</strong></span>
	
		';
	}
	if(get_field('t_texte')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Texts').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_texte').'</strong></span>
	
		';
	}
	if(get_field('t_images')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Images').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_images').'</strong></span>
	
		';
	}
	if(get_field('t_montage')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Editing').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_montage').'</strong></span>
	
		';
	}
	if(get_field('t_mixage')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Sound mix').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_mixage').'</strong></span>
	
		';
	}if(get_field('t_voix')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Voice').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_voix').'</strong></span>
	
		';
	}
	if(get_field('t_son')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Sound Engineer').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_son').'</strong></span>
	
		';
	}
	if(get_field('t_musique')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Music').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_musique').'</strong></span>
	
		';
	}
	if(get_field('t_direction')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Director of production').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_direction').'</strong></span>
	
		';
	}
	if(get_field('t_assproduction')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Production assistant').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_assproduction').'</strong></span>
	
		';
	}
		if(get_field('t_traduction')!=''){
		$the_team_data.= '<span style="color: #808080;">'.__('Translation').'</span>
	<span style="color: #808080;"><strong>'.get_field('t_traduction').'</strong></span>
	
		';
	}
	
	$the_team_data.= '<span style="color: #808080;"><strong>Creative Commons Licence :</strong></span>
	<span style="color: #808080;">Attribution Non-Commercial</span>
	<span style="color: #808080;">No Derivative</span>
	<span style="color: #808080;"><img src="../../../wp-content/themes/plancton-child/images/byncnd.svg" style="width:120px;padding-top: 20px;"></span>';
		
	/** Custom TONTON Portfolio content based VC shortcodes populated by ACF Data **/
	$the_content = '
[vc_row unlock_row_content="yes" row_height_percent="0" override_padding="yes" h_padding="0" top_padding="0" bottom_padding="0" overlay_alpha="50" equal_height="yes" gutter_size="0" shift_y="0" el_class="header-episode border-303030-top"]
	[vc_column column_width_percent="100" position_vertical="top" override_padding="yes" column_padding="0" style="dark" font_family="font-377884" overlay_alpha="50" gutter_size="0" medium_width="0" shift_x="0" shift_y="0" z_index="0" desktop_visibility="yes" medium_visibility="yes" width="1/1"]
		<div class="uncode-single-media text-left animate_when_almost_visible alpha-anim start_animation border-303030-top">
			<div class="single-wrapper" style="max-width: 100%;padding-top: 56.1666666667%;">
				<div class="uncode-single-media-wrapper single-other">
					<iframe src="https://player.vimeo.com/video/'.get_field('id_vimeo').'" width="840" height="473" frameborder="0" title="'.get_the_title().'" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
				</div>
			</div>
		</div>
	[/vc_column]
	[vc_column column_width_percent="100" position_vertical="bottom" override_padding="yes" column_padding="0" style="dark" overlay_alpha="50" gutter_size="0" medium_width="0" shift_x="0" shift_y="0" z_index="0" width="1/3" el_class="teaser-episode "]
		'.$featured_media_html.'
		[vc_column_text el_class="title-episode"]
			<h1>'.get_the_title().'</h1>
			<span style="color: #868690;">'.get_field('sous_titre').'</span>
		[/vc_column_text]
		[vc_column_text el_class="intro-episode"]
			<h4>'.get_field('intro').'</h4>
		[/vc_column_text]
		'.$navigation_content.'
	[/vc_column]
	[vc_column column_width_percent="100" position_vertical="top" override_padding="yes" column_padding="0" style="dark" font_family="font-377884" overlay_alpha="50" gutter_size="0" medium_width="0" shift_x="0" shift_y="0" z_index="0" mobile_visibility="yes" width="2/3" el_class="border-303030-left"]
		<div class="uncode-single-media text-left animate_when_almost_visible alpha-anim start_animation border-303030-top">
			<div class="single-wrapper" style="max-width: 100%;padding-top: 56.1666666667%;">
				<div class="uncode-single-media-wrapper single-other">
					<iframe src="https://player.vimeo.com/video/'.get_field('id_vimeo').'#t=0m10s" width="840" height="473" frameborder="0" title="'.get_the_title().'" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
				</div>
			</div>
		</div>
	[/vc_column]
[/vc_row]

[vc_row unlock_row_content="yes" row_height_percent="0" override_padding="yes" h_padding="0" top_padding="0" bottom_padding="0" overlay_alpha="50" equal_height="yes" gutter_size="0" shift_y="0"]
	[vc_column column_width_percent="100" override_padding="yes" column_padding="0" style="dark" overlay_alpha="50" gutter_size="3" medium_width="0" shift_x="0" shift_y="0" z_index="0" mobile_visibility="yes" width="1/3" el_class="oragnisms-episode padding-5 border-303030-top"]
		[vc_column_text]<h3>'.__('In this episode').'</h3>[/vc_column_text]
		[vc_empty_space empty_h="2"]
		[vc_accordion]'.$episode_related_data.'[/vc_accordion]
	[/vc_column]
	[vc_column column_width_percent="100" override_padding="yes" column_padding="0" style="dark" font_family="font-377884" overlay_alpha="50" gutter_size="3" medium_width="0" shift_x="0" shift_y="0" z_index="0" width="2/3" el_class="details-episode border-303030-top"]
		[vc_row_inner row_inner_height_percent="0" overlay_alpha="50" gutter_size="4" shift_y="0" el_class="gallery-episode padding-5"]
			[vc_column_inner column_width_percent="100" override_padding="yes" column_padding="0" style="light" gutter_size="3" overlay_alpha="50" medium_width="0" shift_x="0" shift_y="0" z_index="0"]
				[vc_column_text]<h3>'.__('Photos').'</h3>[/vc_column_text]
				[vc_gallery el_id="gallery-958170" type="carousel" medias="'.$galerie_photos.'" carousel_lg="9" carousel_md="9" carousel_sm="4" thumb_size="sixteen-nine" gutter_size="1" media_items="media|lightbox|original,icon,title" carousel_height="equal" carousel_interval="3000" carousel_navspeed="400" carousel_nav_skin="dark" carousel_autoh="yes" single_back_color="color-jevc" single_overlay_opacity="15" single_text_visible="yes" single_overlay_visible="yes" single_padding="2" single_border="yes" lbox_title="yes" lbox_caption="yes" lbox_social="yes" lbox_no_tmb="yes" title="Galerie Photo" items="eyI0NDgxNV9pIjp7InNpbmdsZV9sYXlvdXQiOiJtZWRpYXxsaWdodGJveHxvcmlnaW5hbCxpY29uLHRpdGxlLGNhcHRpb24sZGVzY3JpcHRpb24ifX0="]
			[/vc_column_inner]
		[/vc_row_inner]

		
		[vc_row_inner row_inner_height_percent="0" overlay_alpha="50" gutter_size="4" shift_y="0" el_class="desc-episode padding-5"]
			[vc_column_inner column_width_percent="100" override_padding="yes" column_padding="0" style="light" gutter_size="3" overlay_alpha="50" medium_width="0" shift_x="0" shift_y="0" z_index="0" width="2/3"]
				[vc_column_text]
					<h3>'.__('Narration').'</h3>
				[/vc_column_text]
				[vc_column_text el_class="fontsize-160000"]'.get_field('narration').'[/vc_column_text]
				[uncode_share layout="multiple" bigger="yes" css_animation="alpha-anim" title="'.__('Share this on').'"]
			[/vc_column_inner]

			[vc_column_inner column_width_percent="100" position_horizontal="right" position_vertical="top" align_horizontal="align_right" style="light" gutter_size="3" overlay_alpha="50" medium_width="0" shift_x="0" shift_y="0" align_mobile="align_left_mobile" z_index="0" width="1/3" css=".vc_custom_1452121577723{padding-right: 4% !important;}"]
				[vc_column_text el_class="small"]
					'.$the_team_data.'
				[/vc_column_text]
			[/vc_column_inner]
		[/vc_row_inner]

	[/vc_column]
[/vc_row]
[vc_row unlock_row_content="yes" row_height_percent="0" override_padding="yes" h_padding="0" top_padding="0" bottom_padding="0" overlay_alpha="50" equal_height="yes" gutter_size="0" shift_y="0" el_class="videos-reliees-episode padding-5-top"]
	[vc_column column_width_percent="100" align_horizontal="align_center" position_vertical="middle" override_padding="yes" column_padding="0" style="dark" font_family="font-377884" overlay_alpha="50" gutter_size="0" medium_width="0" shift_x="0" shift_y="0" z_index="0" el_class="no-vectical-padding" width="1/1"]
		[vc_empty_space empty_h="1"]
		[vc_column_text]
			<h3>'.__('Related Videos').'</h3>
		[/vc_column_text]
	[/vc_column]
[/vc_row]	

[vc_row unlock_row_content="yes" row_height_percent="0" override_padding="yes" h_padding="0" top_padding="0" bottom_padding="0" overlay_alpha="50" equal_height="yes" gutter_size="0" shift_y="0" el_class="padding-5"]
	[vc_column column_width_percent="100" override_padding="yes" column_padding="0" style="dark" overlay_alpha="50" gutter_size="3" medium_width="0" shift_x="0" shift_y="0" z_index="0" width="1/1" el_class="no-vectical-padding" ]
		
		[uncode_index 
			el_id="index-1" 
			index_type="carousel" 
			loop="size:24|order_by:rand|order:DESC|post_type:portfolio|tax_query:'.$cats_related.'" 
			carousel_lg="7" carousel_md="4" carousel_sm="2" gutter_size="2" post_items="title,media|featured|onpost,category" portfolio_items="media|featured|onpost|original,title,icon" page_items="title,media,text,category" product_items="title,media,text,category,price" carousel_interval="3000" carousel_navspeed="400" carousel_nav="yes" single_style="dark" single_overlay_opacity="1" single_image_color_anim="yes" single_h_align="center" single_padding="2" single_text_reduced="yes" single_title_dimension="h4" single_title_family="font-762333" single_title_weight="100" single_border="yes" single_css_animation="zoom-in" single_animation_speed="500" filtering_menu="inline" single_block_click="yes" single_text_hover="yes" single_no_background="yes" single_title_serif="" single_title_divider="" single_half_padding="yes" single_title_bold="yes" single_image_size="three-four" footer_position="left" carousel_rtl="" single_icon="fa fa-play" filtering_transform="uppercase" items="e30="]
	[/vc_column]
[/vc_row]
[vc_row unlock_row_content="yes" row_height_percent="0" override_padding="yes" h_padding="0" top_padding="0" bottom_padding="0" overlay_alpha="50" equal_height="yes" gutter_size="0" shift_y="0" el_class="videos-reliees"]
	[vc_column column_width_percent="100" override_padding="yes" column_padding="0" style="dark" font_family="font-377884" overlay_alpha="50" gutter_size="3" medium_width="0" shift_x="0" shift_y="0" z_index="0" el_class="no-vectical-padding"  width="1/1"]
		'.$navigation_content.'
	[/vc_column]
[/vc_row]';
	$text_content = apply_filters('the_content', get_the_excerpt());

	/** Build media **/

	if ($media !== '' && !$with_builder && $show_media)
	{
		if ($layout === 'sidebar_right' || $layout === 'sidebar_left')
		{
			$media_size = 12 - $sidebar_size;
		}
		else $media_size = 12;

		$media_array = explode(',', $media);
		$media_counter = count($media_array);
		$rand_id = big_rand();
		if ($media_counter === 0) $media_array = array(
			$media
		);

		if ($media_display === 'isotope') $media_content.=
				'<div id="gallery-' . $rand_id . '" class="isotope-system">
					<div class="isotope-wrapper half-gutter">
      			<div class="isotope-container isotope-layout style-masonry" data-type="masonry" data-layout="masonry" data-lg="1000" data-md="600" data-sm="480">';

		foreach ($media_array as $key => $value)
		{
			if ($media_display === 'carousel') $value = $media;
			$block_data = array();
			$block_data['tmb_data'] = array();
			$block_layout['media'] = array();
			$block_layout['icon'] = array();
			$block_data['title_classes'] = array();
			$block_data['media_id'] = $value;
			$block_data['classes'] = array(
				'tmb'
			);
			$block_data['text_padding'] = 'no-block-padding';
			if ($media_display === 'isotope')
			{
				$block_data['single_width'] = 4;
				$block_data['classes'][] = 'tmb-iso-w4';
			}
			else $block_data['single_width'] = $media_size;
			$block_data['single_style'] = $style;
			$block_data['classes'][] = 'tmb-' . $style;
			if ($media_display === 'isotope')
			{
				$block_data['classes'][] = 'tmb-overlay-anim';
				$block_data['classes'][] = 'tmb-overlay-text-anim';
				$block_data['single_icon'] = 'fa fa-plus2';
				$block_data['overlay_color'] = ($style == 'light') ? 'style-dark-bg' : 'style-light-bg';
				$block_data['overlay_opacity'] = '50';
				$lightbox_classes = array();
				$lightbox_classes['data-noarr'] = false;
			}
			else
			{
				$lightbox_classes = false;
				$block_data['link_class'] = 'inactive-link';
				$block_data['link'] = '#';
			}
			if ($media_display !== 'carousel')
			{
				$block_data['classes'][] = 'tmb-media';
				$block_data['animation'] = ' animate_when_almost_visible alpha-anim';
				$block_data['tmb_data']['data-delay'] = 200;
			}
			$media_html = uncode_create_single_block($block_data, $rand_id, 'masonry', $block_layout, $lightbox_classes, false, true);
			if ($media_display !== 'isotope') $media_content.= '<div class="post-media' . ((($media_counter - 1) !== $key && $media_display === 'stack') ? ' single-bottom-padding' : '') . '">' . $media_html . '</div>';
			else
			{
				$media_content.= $media_html;
			}
			if ($media_display === 'carousel') break;
		}

		if ($media_display === 'isotope') $media_content.=
					'</div>
				</div>
			</div>';
	}

	/** Build post footer **/

	$footer_content = '<div class="post-share">
          							<div class="detail-container">
													<span class="detail-label">' . esc_html__('Share', 'uncode') . '</span>
													<div class="share-button share-buttons share-inline only-icon"></div>
												</div>
										</div>';

	if ($layout === 'sidebar_right' || $layout === 'sidebar_left')
	{

		/** Build structure with sidebar **/

		if ($sidebar_size === '' || empty($sidebar_size)) $sidebar_size = 4;
		$main_size = 12 - $sidebar_size;
		$expand_col = '';

		if ($with_builder) $the_content = $media_content . $the_content;
		else
		{
			if ($the_content !== '' && !empty($the_content))
			{
				$the_content = uncode_get_row_template($the_content, '', '', $style, ' limit-width', ($media_content !== '' ? true : 'double') , false, 'double');
				$the_content = '<div class="post-content">' . $the_content . '</div>';
				if ($media_content !== '') $the_content = uncode_get_row_template($media_content, '', $limit_content_width, $style, '', false, false, false) . $the_content;
			}
			else
			{
				if ($media_content !== '') $media_content = uncode_get_row_template($media_content, '', $limit_content_width, $style, '', false, false, 'double');
				$the_content = $media_content . $the_content;
			}
		}

		/** Collect paddings data **/

		$footer_classes = ' no-bottom-padding';

		if ($portfolio_bg_color !== '')
		{
			if ($sidebar_fill === 'on')
			{
				$sidebar_inner_padding.= ' std-block-padding';
				$sidebar_padding.= $portfolio_bg_color;
				$expand_col = ' unexpand';
				$media_content = str_replace(' single-bottom-padding', '', $media_content);
				if ($limit_content_width === '')
				{
					$row_classes.= ' no-h-padding col-no-gutter no-top-padding';
					$footer_classes = ' single-top-padding';
					if (!$with_builder)
					{
						$main_classes.= ' std-block-padding';
					}
				}
				else
				{
					$row_classes.= ' no-top-padding';
					if (!$with_builder)
					{
						$main_classes.= ' double-top-padding';
					}
				}
			}
			else
			{
				$row_classes.= ' double-top-padding';
				$sidebar_inner_padding.= $portfolio_bg_color . ' single-block-padding';
			}
		}
		else
		{
			if ($with_builder)
			{
				if ($limit_content_width === '')
				{
					$row_classes.= ' col-half-gutter no-top-padding no-h-padding';
					$sidebar_inner_padding.= ' double-top-padding single-block-padding';
				}
				else
				{
					$row_classes.= ' col-std-gutter no-top-padding';
					$sidebar_inner_padding.= ' double-top-padding';
				}
			}
			else
			{
				$row_classes.= ' col-std-gutter double-top-padding';
				$main_classes.= ' double-bottom-padding';
			}
		}

		$row_classes.= ' no-bottom-padding';
		$sidebar_inner_padding.= ' double-bottom-padding';

		/** Create html with sidebar **/

		$footer_content = '<div class="post-footer post-footer-' . $style . ' style-' . $style . $footer_classes . '">' . $footer_content . '</div>';

		$main_content = '<div class="col-lg-' . $main_size . '">
									' . $the_content . '
								</div>';

		$info_content = '<div class="info-content">' . $title_content . $text_content . '<hr />' . $info_content . '</div>';

		$the_content = '<div class="row-container">
			        				<div class="row row-parent' . $row_classes . $limit_content_width . '">
												<div class="row-inner">
													' . (($layout === 'sidebar_right') ? $main_content : '') . '
													<div class="col-lg-' . $sidebar_size . '">
														<div class="uncol style-' . $portfolio_style . $expand_col . $sidebar_padding . '">
															<div class="uncoltable">
																<div class="uncell' . $sidebar_inner_padding . '">
																	<div class="uncont">
																		' . $info_content . $footer_content . '
																	</div>
																</div>
															</div>
														</div>
													</div>
													' . (($layout === 'sidebar_left') ? $main_content : '') . '
												</div>
											</div>
										</div>';
	}

	if ($layout === 'portfolio_top' || $layout === 'portfolio_bottom')
	{

		/** Create html without sidebar **/

		if ($with_builder) $the_content = $media_content . $the_content;
		else
		{
			if ($the_content !== '' && !empty($the_content))
			{
				$the_content = uncode_get_row_template($the_content, '', '', $style, ' limit-width', ($layout === 'portfolio_top' ? false : 'double') , false, ($layout === 'portfolio_top' ? 'double' : false));
				$the_content = '<div class="post-content">' . $the_content . '</div>';
			}
			if ($media_content !== '') $media_content = uncode_get_row_template($media_content, '', $limit_content_width, $style, '', ($layout === 'portfolio_top' && $portfolio_bg_color === '' ? false : 'double') , true, ($layout === 'portfolio_bottom' && $portfolio_bg_color === '' ? false : 'double'));
			$the_content = $media_content . $the_content;
		}

		$footer_content = '<div class="post-footer">' . $footer_content . '</div>';

		if ($title_content !== '')
		{
			$title_content = '<div class="row-inner">
													<div class="col-lg-12">
														<div class="uncont">
															' . $title_content . '
														</div>
													</div>
												</div>';
		}

		$info_content = '<div class="row-portfolio-info row-container style-' . $portfolio_style . $portfolio_bg_color . '">
        							<div class="row row-parent col-std-gutter limit-width double-top-padding double-bottom-padding' . $info_classes . '">
        								' . $title_content . '
												<div class="row-inner">
													<div class="col-lg-8">
														<div class="uncol">
															<div class="uncont">
																' . $text_content . '
															</div>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="uncol">
															<div class="uncont">
																<div class="info-content">
																	' . $info_content . '
																</div>
																' . $footer_content . '
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>';

		if ($layout === 'portfolio_top')
		{
			$the_content = $info_content . $the_content;
		}
		else
		{
			$the_content = $the_content . $info_content;
		}
	}

	/** Display post html **/
	if ($layout === '')
	{
		if ($with_builder) $the_content = $media_content . $the_content;
		else
		{
			// TONTON Force full-width layout
			if ($title_content !== '' || $the_content !== '') $the_content = uncode_get_row_template($title_content . $the_content, '', '', $style, ' full-width', ($media_content === '' ? 'double' : false), false, 'double');
			if ($media_content !== '') $media_content = uncode_get_row_template($media_content, '', $limit_content_width, $style, '', 'double', true, 'double');
			$the_content = $media_content . $the_content;
		}
		$the_content = '<div class="post-content">' . $the_content . '</div>';
	}

	

	echo '<div class="page-body' . $bg_color . '">
			<div class="portfolio-wrapper"' . $page_custom_width . '>
				<div class="portfolio-body">' . do_shortcode($the_content) . '</div>' .
			'</div>
		</div>';
endwhile;
// end of the loop.

get_footer(); ?>